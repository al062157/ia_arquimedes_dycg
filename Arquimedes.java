package arquimedes;

import java.util.Scanner;

public class Arquimedes {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Ingrese el numero de lados");
        int lados = entrada.nextInt();
        double arreglo [] = new double [10];
        int arregloLado[] = new int [10];
        
        float ladoInicial =1;
        for (int i = 0; i < 10; i++) {
            arregloLado[i]= lados;
            float x= ladoInicial/2;
            float a= (float) Math.sqrt(1-Math.pow(x, 2));
            float b = 1-a;
            float nuevoLado = (float) Math.sqrt(Math.pow(x, 2)+ Math.pow(b, 2));
            float P= ladoInicial*lados;
            float PD= P/2;
            ladoInicial = nuevoLado;
            
            lados =lados*2;
            arreglo[i] = PD;
        }
        
        for (int i = 0; i < 10; i++) {
            System.out.println("N° de lado " + arregloLado[i] + ", " + arreglo[i]);
        }


        
    }
    
}
